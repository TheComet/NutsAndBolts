// (c) 2015 Evan Lindsay
// This code is licensed under MIT license; see LICENSE.txt for details.
// https;//github.com/Favorlock

// Dependencies
var config = require('config');
var fs = require('fs');
var mkdirp = require('mkdirp');
var util = require('util');
var http = require('http');
var readline = require('readline');

// Project Dependencies
var loop = require('./lib/loop')
var appInfo = require('./package.json')

// Console Input
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

start()

function start() {
    console.info('Starting ' + appInfo.name + ' v' + appInfo.version)
    // Application logic loop
    var loopCount = 0;
    var id = loop.setLoop(function(delta) {
        fetchSources();
    }, 1000 * 60 * 15)

    rl.setPrompt("");
    rl.prompt();
    rl.on('line', function(line) {
        handle(line);
    }).on('close', function() {
        exit();
    })
}

function handle(line) {
    switch (line.trim()) {
        case 'version':
            console.info(appInfo.name + ' v' + appInfo.version);
            break;
        case 'fetch':
            console.info('Fetching artifacts from TeamCity CI');
            fetchSources();
            break;
        case 'exit':
        case 'stop':
        case 'quit':
            exit();
            break;
    }

    rl.prompt();
}

function exit() {
    console.info('Exiting NutsAndBolts!');
    process.exit(0);
}

function fetchSources () {
    getConfigurations().forEach(function (configuration_key) {
        var configuration = config.get("configurations." + configuration_key);
        var destination = configuration.destination;
        if (destination.substr(-1) !== '/') {
            destination += "/"
        }
        var sources = getSources(configuration);

        sources.forEach(function (source_key) {
            var artifacts = config.get("configurations." + configuration_key + ".sources." + source_key);
            fetchArtifacts(destination, source_key, artifacts);
        });
    })
}

function fetchArtifacts (destination, source, artifacts) {
    artifacts.forEach(function (artifact) {
        var options = createOptions(source, artifact);

        http.get(options, function(res) {
            if (res.statusCode == '200') {
                if (!fs.existsSync(destination)) {
                    mkdirp.sync(destination);
                }

                var file = fs.createWriteStream(destination + artifact);
                res.pipe(file);
            } else {
                console.error('Failed to retrieve artifact from source: ' + source);
            }
        });
    })
}

function getConfigurations () {
    configurations = [];
    for (var property in config.get('configurations')) {
        configurations[configurations.length] = property;
    }
    return configurations;
}

function getSources (configuration) {
    sources = [];
    for (var property in configuration.sources) {
        sources[sources.length] = property;
    }
    return sources;
}

function createOptions(buildType, artifact) {
    return {
        host: config.get('ci.host'),
        port: config.get('ci.port'),
        path: util.format("/httpAuth/app/rest/builds/buildType:%s,pinned:true/artifacts/content/%s", buildType, artifact),
        method: 'GET',
        headers: {
            'Authorization': 'Basic ' + new Buffer(config.get('ci.username') + ':' + config.get('ci.password')).toString('base64')
        }
    };
}