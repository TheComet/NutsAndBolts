var activeLoops = [];

var getLoopId = (function() {
    var staticLoopId = 0;
    return function() {
        return staticLoopId++;
    }
})

module.exports.setLoop = function(update, tickLengthMs) {
    var loopId = getLoopId();
    activeLoops.push(loopId);

    tickLengthMs = tickLengthMs || 1000 / 30;

    var previousTick = Date.now();
    var actualTicks = 0;

    var gameLoop = function() {
        var now = Date.now();

        actualTicks++
        if (previousTick + tickLengthMs <= now) {
            var delta = (now - previousTick) / 1000;
            previousTick = now;

            update(delta);

            actualTicks = 0;
        }

        if (activeLoops.indexOf(loopId) === -1) {
            return;
        }

        if (Date.now() - previousTick < tickLengthMs - 16) {
            setTimeout(gameLoop, 16);
        } else {
            setImmediate(gameLoop);
        }
    }

    gameLoop();

    return loopId;
}

module.exports.clearLoop = function(loopId) {
    activeLoops.splice(activeLoops.indexOf(loopId), 1);
}